#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include "Menu1.h"



int a = 1920;
int b = 1080;
int c = 400;
int d = 288;
int selectLevel = 0;

using namespace sf;
using namespace std;

bool isGame = true;

int level = 1;

const int H = 9;
const int W = 14;


String TileMap0[H] = {

	"BBBBBBBBBBBBBB",
	"BBGg B  KB rRB",
	"BB  BB C BB  B",
	"BBBB  Ss   BBB",
	"BBBB   k   BBB",
	"BBBB   P   BBB",
	"BB  BB   BB  B",
	"BBBBBBBBBBBBBB",
	"BBBBBBBBBBBBBB",

};

String TileMap[H] = {

	"BBBBBBBBBBBBBB",
	"BBGg B  KB rRB",
	"BB  BB C BB  B",
	"BBBB  Ss   BBB",
	"BBBB   k   BBB",
	"BBBB   P   BBB",
	"BB  BB   BB  B",
	"BBBBBBBBBBBBBB",
	"BBBBBBBBBBBBBB",

};


String TileMap2[H] = {

	"BBBBBBBBBBBBBB",
	"BB B    P BB B",
	"BB B  BkK  B B",
	"BB BB  s   B B",
	"BB BB   S BB B",
	"BB   BRr B   B",
	"BB   B CBGg  B",
	"BBBBBBBBBBBBBB",
	"BBBBBBBBBBBBBB",

};

String TileMap3[H] = {

	"BBBBBBBBBBBBBB",
	"BBS   BBB  B B",
	"BB   B   G B B",
	"BB  B k rR B B",
	"BB  B      B B",
	"BB   BBKgBBB B",
	"BB    BPCB   B",
	"BBs   B  B   B",
	"BBBBBBBBBBBBBB",

};

String TileMap4[H] = {

	"BBBBBBBBBBBBBB",
	"BB BBB       B",
	"BBB   BBB    B",
	"BB s S   B   B",
	"BB Rg BK B   B",
	"BB  rG kPB   B",
	"BBB C B  B   B",
	"BBBBBBBBBBBBBB",
	"BBBBBBBBBBBBBB",

};
String TileMap5[H] = {

	"BBBBBBBBBBBBBB",
	"BB B CBB  Ss B",
	"BB BP K B    B",
	"BBBBkr   B   B",
	"BB   gBR B   B",
	"BB  G    B   B",
	"BB  BBB  B   B",
	"BBBBBBBBBBBBBB",
	"BBBBBBBBBBBBBB",

};
String TileMap6[H] = {

	"BBBBBBBBBBBBBB",
	"BB BB BBB    B",
	"BBB  B C B   B",
	"BBBP R s B   B",
	"BB  KgSr B   B",
	"BB G  k  B   B",
	"BBBBB  B B   B",
	"BBBBB    B   B",
	"BBBBBBBBBBBBBB",

};
String TileMap7[H] = {

	"BBBBBBBBBBBBBB",
	"BB    BBB    B",
	"BB   B   BBBBB",
	"BB   BRgks   B",
	"BB   B  GKrC B",
	"BB    BBBS   B",
	"BB     B P BBB",
	"BBBBBBBB   BBB",
	"BBBBBBBBBBBBBB",

};
String TileMap8[H] = {

	"BBBBBBBBBBBBBB",
	"BB   BBBB    B",
	"BBBBB    B   B",
	"BB PK    B   B",
	"BB C rkBBB   B",
	"BBBBgSG  B   B",
	"BB   BsB B   B",
	"BB   R   BBBBB",
	"BBBBBBBBBBBBBB",

};
String TileMap9[H] = {

	"BBBBBBBBBBBBBB",
	"BBBB   B G BBB",
	"BB B  S   BB B",
	"BB BBB s RBB B",
	"BB B   BBgBB B",
	"BB B kPK r B B",
	"BB BBB   C B B",
	"BBBBBBBBBBBBBB",
	"BBBBBBBBBBBBBB",

};
String TileMap10[H] = {

	"BBBBBBBBBBBBBB",
	"BBB  BBB     B",
	"BBBG B  B    B",
	"BB   BkRB    B",
	"BBBrS  KPB   B",
	"BBB  sg CB   B",
	"BBB   B  B   B",
	"BBBBBBBBBBBBBB",
	"BBBBBBBBBBBBBB",

};


RenderWindow window;
Image image;
Image imagek;
Image imagek2;
Image imagep1;
Image imagep2;
Image imagep3;
Image imagePillarBlue;
Image imagePillarRed;
Image imagePillarGrey;
Image imageCoin;
Texture texture, texturek, texturek2, texturep3, texturep2, texturep1, texturePillarBlue, texturePillarRed, texturePillarGrey, textureCoin;
Sprite sprite, spritek, spritek2, spritep3, spritep2, spritep1, spritePillarBlue, spritePillarRed, spritePillarGrey, spriteCoin;
bool isMenu1 = true;
bool isMenu = true;
bool isMenuOptions1 = false;
bool isMenuOptions = false;
bool unWindow = false;



void menuOptions1(RenderWindow & window) {

	MenuOptions menuOptions(800, 450);
	Texture textureback;
	textureback.loadFromFile("back.png");
	Sprite back(textureback);

	while (isMenuOptions == true)
	{
		sf::View view(sf::FloatRect(0, 0, 800, 450));
		window.setView(view);
		Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyReleased:
				switch (event.key.code)
				{
				case sf::Keyboard::Up:
					menuOptions.MoveUP();
					break;
				case sf::Keyboard::Down:
					menuOptions.MoveDown();
					break;
				case sf::Keyboard::Return:
					switch (menuOptions.getpresseditem())
					{
					case 0:
						::selectLevel = 1;
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = true;
						break;
					case 1:
						::selectLevel = 2;
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = true;
						break;
					case 2:
						::selectLevel = 3;
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = true;
						break;
					case 3:
						selectLevel = 4;
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = true;
						break;
					case 4:
						selectLevel = 5;
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = true;
						break;
					case 5:
						selectLevel = 6;
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = true;
						break;
					case 6:
						selectLevel = 7;
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = true;
						break;
					case 7:
						selectLevel = 8;
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = true;
						break;
					case 8:
						selectLevel = 9;
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = true;
						break;
					case 9:
						selectLevel = 10;
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = true;
						break;
					case 10:
						isMenu1 = false;
						isMenu = false;
						isMenuOptions1 = false;
						isMenuOptions = false;
						isGame = false;
						window.close();
						break;
					case 11:
						isMenu1 = true;
						isMenu = true;
						isMenuOptions1 = false;
						isMenuOptions = false;
						break;
						break;

					}
					break;
				}
				break;
			}

			if (event.type == Event::Closed)
				window.close();
		}
		window.clear();
		window.draw(back);
		menuOptions.draw(window);
		window.display();
	}

}

void menu1(RenderWindow & window) {

	Menu1 menu(800, 450);
	Texture aboutTexture, rulesTexture, textureback;
	aboutTexture.loadFromFile("about.png");
	rulesTexture.loadFromFile("rules.png");
	textureback.loadFromFile("back.png");
	Sprite about(aboutTexture);
	Sprite rules(rulesTexture);
	Sprite back(textureback);
	while (isMenu == true)
	{
		sf::View view(sf::FloatRect(0, 0, 800, 450));
		window.setView(view);
		Event event;
		while (window.pollEvent(event))
		{


			switch (event.type)
			{
			case sf::Event::KeyReleased:
				switch (event.key.code)
				{
				case sf::Keyboard::Up:
					menu.MoveUP();
					break;
				case sf::Keyboard::Down:
					menu.MoveDown();
					break;
				case sf::Keyboard::Return:
					switch (menu.getpresseditem())
					{
					case 0:
						isMenu1 = false;
						isMenu = false;
						isGame = true;
						break;
					case 1:
						isMenuOptions1 = true;
						isMenuOptions = true;
						isMenu1 = false;
						isMenu = false;
						break;
					case 2:
					{window.draw(rules); window.display(); while (!Keyboard::isKeyPressed(Keyboard::Escape)); }
					break;
					case 3:
					{window.draw(about); window.display(); while (!Keyboard::isKeyPressed(Keyboard::Escape)); }
					break;
					case 4:
						isMenu1 = false;
						isMenu = false;
						isGame = false;
						window.close();
						break;

					}
					break;
				}
				break;
			}

			if (event.type == Event::Closed)
				window.close();
		}

		window.clear();
		window.draw(back);
		menu.draw(window);
		window.display();
	}

}




int speed = 1;
int HeroDirection;

int main()
{
	RenderWindow window(VideoMode(a, b), "REBUS", sf::Style::Fullscreen);

	window.setFramerateLimit(60);
	window.setMouseCursorVisible(false);



	sf::SoundBuffer soundBuffer;
	sf::Sound sound;
	if (!soundBuffer.loadFromFile("lvlup.ogg")) cout << "no sound";
	sound.setBuffer(soundBuffer);
	sound.setVolume(30);

	sf::SoundBuffer soundBuffer2;
	sf::Sound sound2;
	if (!soundBuffer2.loadFromFile("ruhkamnia2.ogg")) cout << "no sound";
	sound2.setBuffer(soundBuffer2);

	sf::SoundBuffer soundBuffer3;
	sf::Sound sound3;
	if (!soundBuffer3.loadFromFile("pl_dirt1.ogg")) cout << "no sound";
	sound3.setBuffer(soundBuffer3);
	sound3.setVolume(18);

	sf::SoundBuffer soundBuffer5;
	sf::Sound sound5;
	if (!soundBuffer5.loadFromFile("pl_dirt3.ogg")) cout << "no sound";
	sound5.setBuffer(soundBuffer5);
	sound5.setVolume(18);



	int Px, Py, Sx, Sy, sx, sy, Kx, Ky, kx, ky, Rx, Ry, rx, ry, Gx, Gy, gx, gy, Cx, Cy;




	image.loadFromFile("ash2.png");
	texture.loadFromImage(image);
	sprite.setTexture(texture);
	sprite.setTextureRect(IntRect(0, 0, 32, 32));

	imageCoin.loadFromFile("stina.png");
	textureCoin.loadFromImage(imageCoin);
	spriteCoin.setTexture(textureCoin);
	spriteCoin.setTextureRect(IntRect(63, 0, 32, 32));

	imagek.loadFromFile("kamin.png");
	texturek.loadFromImage(imagek);
	spritek.setTexture(texturek);
	spritek.setTextureRect(IntRect(0, 0, 32, 32));

	spritek2.setTexture(texturek);
	spritek2.setTextureRect(IntRect(0, 0, 32, 32));

	imagep1.loadFromFile("ppd.png");
	texturep1.loadFromImage(imagep1);
	spritep1.setTexture(texturep1);
	spritep1.setTextureRect(IntRect(64, 0, 32, 32));

	spritep2.setTexture(texturep1);
	spritep2.setTextureRect(IntRect(64, 32, 32, 32));

	spritep3.setTexture(texturep1);
	spritep3.setTextureRect(IntRect(64, 64, 32, 32));


	imagePillarBlue.loadFromFile("ppd.png");
	texturePillarBlue.loadFromImage(imagePillarBlue);
	spritePillarBlue.setTexture(texturePillarBlue);
	spritePillarBlue.setTextureRect(IntRect(0, 0, 32, 32));


	spritePillarRed.setTexture(texturePillarBlue);
	spritePillarRed.setTextureRect(IntRect(0, 32, 32, 32));


	spritePillarGrey.setTexture(texturePillarBlue);
	spritePillarGrey.setTextureRect(IntRect(0, 64, 32, 32));



	bool MoveLeft, MoveRight, MoveUp, MoveDown, PillarBlueUp, PillarRedUp, PillarGreyUp,
		allowMoveBlueUp, allowMoveBlueDown, allowMoveBlueLeft, allowMoveBlueRight,
		allowMoveRedUp, allowMoveRedDown, allowMoveRedLeft, allowMoveRedRight,
		allowMoveGreyUp, allowMoveGreyDown, allowMoveGreyLeft, allowMoveGreyRight, ChangeLevel = true, allowCollision = false;
	int MoveLeftInt = 0, MoveRightInt = 0, MoveUpInt = 0, MoveDownInt = 0;
	float Currentframe1 = 0;

	Clock clock;
	Texture s;
	s.loadFromFile("stina.png");
	Sprite st(s);
	Texture t;
	t.loadFromFile("ash2.png");
	Sprite tt(t);
	RectangleShape rectangle(Vector2f(32, 32));




	while (isGame) {


		while (isMenu) {
			menu1(window);
		}
		while (isMenuOptions == true) {
			menuOptions1(window);
		}



		while (isGame == true && isMenu1 == false && isMenuOptions1 == false)
		{

			if (isGame == true && isMenu1 == false && isMenuOptions1 == false) {
				sf::View view(sf::FloatRect(32, 0, c, d));
				window.setView(view);
			}

			float time = clock.getElapsedTime().asMicroseconds();
			clock.restart();
			time = time / 800;
			Event event;
			while (window.pollEvent(event))
			{

				if (Keyboard::isKeyPressed(Keyboard::Escape))
				{
					isMenu1 = true;
					isMenu = true;
				}




			}

			float spritexkposition = spritek.getPosition().x;
			float spriteykposition = spritek.getPosition().y;

			float spritexk2position = spritek2.getPosition().x;
			float spriteyk2position = spritek2.getPosition().y;

			float spritexposition = sprite.getPosition().x;
			float spriteyposition = sprite.getPosition().y;

			float spritep1xposition = spritep1.getPosition().x;
			float spritep1yposition = spritep1.getPosition().y;

			float spritep2xposition = spritep2.getPosition().x;
			float spritep2yposition = spritep2.getPosition().y;

			float spritep3xposition = spritep3.getPosition().x;
			float spritep3yposition = spritep3.getPosition().y;

			float spritePillarBluexposition = spritePillarBlue.getPosition().x;
			float spritePillarBlueyposition = spritePillarBlue.getPosition().y;

			float spritePillarRedxposition = spritePillarRed.getPosition().x;
			float spritePillarRedyposition = spritePillarRed.getPosition().y;

			float spritePillarGreyxposition = spritePillarGrey.getPosition().x;
			float spritePillarGreyyposition = spritePillarGrey.getPosition().y;

			float spriteCoinxposition = spriteCoin.getPosition().x;
			float spriteCoinyposition = spriteCoin.getPosition().y;


			if (Keyboard::isKeyPressed(Keyboard::Left) && MoveLeftInt == 0 && MoveRightInt == 0 && MoveUpInt == 0 && MoveDownInt == 0) { MoveLeft = true; MoveLeftInt = 32; }
			if (Keyboard::isKeyPressed(Keyboard::Right) && MoveLeftInt == 0 && MoveRightInt == 0 && MoveUpInt == 0 && MoveDownInt == 0) { MoveRight = true; MoveRightInt = 32; }
			if (Keyboard::isKeyPressed(Keyboard::Up) && MoveLeftInt == 0 && MoveRightInt == 0 && MoveUpInt == 0 && MoveDownInt == 0) { MoveUp = true; MoveUpInt = 32; }
			if (Keyboard::isKeyPressed(Keyboard::Down) && MoveLeftInt == 0 && MoveRightInt == 0 && MoveUpInt == 0 && MoveDownInt == 0) { MoveDown = true; MoveDownInt = 32; }

			Currentframe1 += 0.25;
			if (Currentframe1 >= 3) { Currentframe1 = 0; }


			if (MoveLeft = true && MoveLeftInt>0)
			{
				HeroDirection = 0;
				sprite.setTextureRect(IntRect(32 * int(Currentframe1) + 32, 100, -32, 32));
				sprite.move(-1, 0);
				allowCollision = true;
				MoveLeftInt--;
			}

			if (MoveRight = true && MoveRightInt>0)
			{

				HeroDirection = 1;
				sprite.setTextureRect(IntRect(32 * int(Currentframe1), 100, 32, 32));
				sprite.move(1, 0);
				allowCollision = true;
				MoveRightInt--;
			}
			if (MoveUp = true && MoveUpInt>0)
			{
				HeroDirection = 2;
				sprite.setTextureRect(IntRect(32 * int(Currentframe1) + 32, 0, -32, 32));
				sprite.move(0, -1);
				allowCollision = true;
				MoveUpInt--;
			}
			if (MoveDown = true && MoveDownInt>0)
			{
				HeroDirection = 3;
				sprite.setTextureRect(IntRect(32 * int(Currentframe1), 32, 32, 32));
				sprite.move(0, 1);
				allowCollision = true;
				MoveDownInt--;
			}

			if (MoveLeftInt>0 || MoveRightInt>0 || MoveUpInt>0 || MoveDownInt>0) {
				if (Currentframe1 == 0) { sound3.play(); }

				if (Currentframe1 == 2) { sound5.play(); }
			}


			FloatRect sprite2Bounds = sprite.getGlobalBounds();
			FloatRect spritekBounds = spritek.getGlobalBounds();
			FloatRect spritek2Bounds = spritek2.getGlobalBounds();
			FloatRect spritep3Bounds = spritep3.getGlobalBounds();
			FloatRect spritep2Bounds = spritep2.getGlobalBounds();
			FloatRect spritep1Bounds = spritep1.getGlobalBounds();
			FloatRect spriteCoinBounds = spriteCoin.getGlobalBounds();

			FloatRect spritePillarBlueBounds = spritePillarBlue.getGlobalBounds();
			FloatRect spritePillarRedBounds = spritePillarRed.getGlobalBounds();
			FloatRect spritePillarGreyBounds = spritePillarGrey.getGlobalBounds();

			if (MoveLeftInt>29 || MoveRightInt>29 || MoveUpInt>29 || MoveDownInt>29) { if (sprite2Bounds.intersects(spritekBounds) || sprite2Bounds.intersects(spritek2Bounds))sound2.play(); }



			if (selectLevel == 1) { level = 1; ChangeLevel = true; allowCollision == false; }
			if (level == -2 && spriteCoinxposition == spritexkposition&&spriteCoinyposition == spriteykposition || level == -2 && spriteCoinxposition == spritexk2position&&spriteCoinyposition == spriteyk2position || selectLevel == 2)
			{
				level = 2; ChangeLevel = true; allowCollision == false;
			}
			if (level == 0 && spriteCoinxposition == spritexkposition&&spriteCoinyposition == spriteykposition || level == 0 && spriteCoinxposition == spritexk2position&&spriteCoinyposition == spriteyk2position || selectLevel == 3)
			{
				level = 3; ChangeLevel = true; allowCollision == false;
			}
			if (level == -3 && spriteCoinxposition == spritexkposition&&spriteCoinyposition == spriteykposition || level == -3 && spriteCoinxposition == spritexk2position&&spriteCoinyposition == spriteyk2position || selectLevel == 4)
			{
				level = 4; ChangeLevel = true; allowCollision == false;
			}
			if (level == -4 && spriteCoinxposition == spritexkposition&&spriteCoinyposition == spriteykposition || level == -4 && spriteCoinxposition == spritexk2position&&spriteCoinyposition == spriteyk2position || selectLevel == 5)
			{
				level = 5; ChangeLevel = true; allowCollision == false;
			}
			if (level == -5 && spriteCoinxposition == spritexkposition&&spriteCoinyposition == spriteykposition || level == -5 && spriteCoinxposition == spritexk2position&&spriteCoinyposition == spriteyk2position || selectLevel == 6)
			{
				level = 6; ChangeLevel = true; allowCollision == false;
			}
			if (level == -6 && spriteCoinxposition == spritexkposition&&spriteCoinyposition == spriteykposition || level == -6 && spriteCoinxposition == spritexk2position&&spriteCoinyposition == spriteyk2position || selectLevel == 7)
			{
				level = 7; ChangeLevel = true; allowCollision == false;
			}
			if (level == -7 && spriteCoinxposition == spritexkposition&&spriteCoinyposition == spriteykposition || level == -7 && spriteCoinxposition == spritexk2position&&spriteCoinyposition == spriteyk2position || selectLevel == 8)
			{
				level = 8; ChangeLevel = true; allowCollision == false;
			}
			if (level == -8 && spriteCoinxposition == spritexkposition&&spriteCoinyposition == spriteykposition || level == -8 && spriteCoinxposition == spritexk2position&&spriteCoinyposition == spriteyk2position || selectLevel == 9)
			{
				level = 9; ChangeLevel = true; allowCollision == false;
			}
			if (level == -9 && spriteCoinxposition == spritexkposition&&spriteCoinyposition == spriteykposition || level == -9 && spriteCoinxposition == spritexk2position&&spriteCoinyposition == spriteyk2position || selectLevel == 10)
			{
				level = 10; ChangeLevel = true; allowCollision == false;
			}

			if (level == 1 || level == -2 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				for (int i = 0; i<H; i++) { TileMap[i] = TileMap0[i]; }
			}
			if (level == 2 || level == 0 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				for (int i = 0; i<H; i++) { TileMap[i] = TileMap2[i]; }
			}
			if (level == 3 || level == -3 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				for (int i = 0; i<H; i++) { TileMap[i] = TileMap3[i]; }
			}
			if (level == 4 || level == -4 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				for (int i = 0; i<H; i++) { TileMap[i] = TileMap4[i]; }
			}
			if (level == 5 || level == -5 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				for (int i = 0; i<H; i++) { TileMap[i] = TileMap5[i]; }
			}
			if (level == 6 || level == -6 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				for (int i = 0; i<H; i++) { TileMap[i] = TileMap6[i]; }
			}
			if (level == 7 || level == -7 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				for (int i = 0; i<H; i++) { TileMap[i] = TileMap7[i]; }
			}
			if (level == 8 || level == -8 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				for (int i = 0; i<H; i++) { TileMap[i] = TileMap8[i]; }
			}
			if (level == 9 || level == -9 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				for (int i = 0; i<H; i++) { TileMap[i] = TileMap9[i]; }
			}
			if (level == 10 || level == -10 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				for (int i = 0; i<H; i++) { TileMap[i] = TileMap10[i]; }
			}


			if (ChangeLevel == true) {
				for (int i = 0; i<H; i++) {
					for (int j = 0; j<W; j++)

					{
						if (TileMap[i][j] == 'B') {}
						if (TileMap[i][j] == 'P') { Px = j * 32; Py = i * 32; }
						if (TileMap[i][j] == 'S') { Sx = j * 32; Sy = i * 32; }
						if (TileMap[i][j] == 's') { sx = j * 32; sy = i * 32; }
						if (TileMap[i][j] == 'K') { Kx = j * 32; Ky = i * 32; }
						if (TileMap[i][j] == 'k') { kx = j * 32; ky = i * 32; }
						if (TileMap[i][j] == 'R') { Rx = j * 32; Ry = i * 32; }
						if (TileMap[i][j] == 'r') { rx = j * 32; ry = i * 32; }
						if (TileMap[i][j] == 'G') { Gx = j * 32; Gy = i * 32; }
						if (TileMap[i][j] == 'g') { gx = j * 32; gy = i * 32; }
						if (TileMap[i][j] == 'C') { Cx = j * 32; Cy = i * 32; }
					}
				} ChangeLevel = false;
			}




			if (level == 1 || level == -2 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				ChangeLevel = true;
				sound.play();
				sprite.setPosition(Px, Py);
				spritek.setPosition(Kx, Ky);
				spritek2.setPosition(kx, ky);
				spritep3.setPosition(Gx, Gy);
				spritep2.setPosition(Rx, Ry);
				spritep1.setPosition(Sx, Sy);
				spritePillarBlue.setPosition(sx, sy);
				spritePillarRed.setPosition(rx, ry);
				spritePillarGrey.setPosition(gx, gy);
				spriteCoin.setPosition(Cx, Cy);
				level = -2;
				selectLevel = 0;
			}


			if (level == 2 || level == 0 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				ChangeLevel = true;
				sound.play();
				sprite.setPosition(Px, Py);
				spritek.setPosition(Kx, Ky);
				spritek2.setPosition(kx, ky);
				spritep3.setPosition(Gx, Gy);
				spritep2.setPosition(Rx, Ry);
				spritep1.setPosition(Sx, Sy);
				spritePillarBlue.setPosition(sx, sy);
				spritePillarRed.setPosition(rx, ry);
				spritePillarGrey.setPosition(gx, gy);
				spriteCoin.setPosition(Cx, Cy);
				level = 0;
				selectLevel = 0;
			}




			if (level == 3 || level == -3 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				ChangeLevel = true;
				sound.play();
				sprite.setPosition(Px, Py);
				spritek.setPosition(Kx, Ky);
				spritek2.setPosition(kx, ky);
				spritep3.setPosition(Gx, Gy);
				spritep2.setPosition(Rx, Ry);
				spritep1.setPosition(Sx, Sy);
				spritePillarBlue.setPosition(sx, sy);
				spritePillarRed.setPosition(rx, ry);
				spritePillarGrey.setPosition(gx, gy);
				spriteCoin.setPosition(Cx, Cy);
				level = -3;
				selectLevel = 0;
			}

			if (level == 4 || level == -4 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				ChangeLevel = true;
				sound.play();
				sprite.setPosition(Px, Py);
				spritek.setPosition(Kx, Ky);
				spritek2.setPosition(kx, ky);
				spritep3.setPosition(Gx, Gy);
				spritep2.setPosition(Rx, Ry);
				spritep1.setPosition(Sx, Sy);
				spritePillarBlue.setPosition(sx, sy);
				spritePillarRed.setPosition(rx, ry);
				spritePillarGrey.setPosition(gx, gy);
				spriteCoin.setPosition(Cx, Cy);
				level = -4;
				selectLevel = 0;
			}

			if (level == 5 || level == -5 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				ChangeLevel = true;
				sound.play();
				sprite.setPosition(Px, Py);
				spritek.setPosition(Kx, Ky);
				spritek2.setPosition(kx, ky);
				spritep3.setPosition(Gx, Gy);
				spritep2.setPosition(Rx, Ry);
				spritep1.setPosition(Sx, Sy);
				spritePillarBlue.setPosition(sx, sy);
				spritePillarRed.setPosition(rx, ry);
				spritePillarGrey.setPosition(gx, gy);
				spriteCoin.setPosition(Cx, Cy);
				level = -5;
				selectLevel = 0;
			}

			if (level == 6 || level == -6 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				ChangeLevel = true;
				sound.play();
				sprite.setPosition(Px, Py);
				spritek.setPosition(Kx, Ky);
				spritek2.setPosition(kx, ky);
				spritep3.setPosition(Gx, Gy);
				spritep2.setPosition(Rx, Ry);
				spritep1.setPosition(Sx, Sy);
				spritePillarBlue.setPosition(sx, sy);
				spritePillarRed.setPosition(rx, ry);
				spritePillarGrey.setPosition(gx, gy);
				spriteCoin.setPosition(Cx, Cy);
				level = -6;
				selectLevel = 0;
			}

			if (level == 7 || level == -7 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				ChangeLevel = true;
				sound.play();
				sprite.setPosition(Px, Py);
				spritek.setPosition(Kx, Ky);
				spritek2.setPosition(kx, ky);
				spritep3.setPosition(Gx, Gy);
				spritep2.setPosition(Rx, Ry);
				spritep1.setPosition(Sx, Sy);
				spritePillarBlue.setPosition(sx, sy);
				spritePillarRed.setPosition(rx, ry);
				spritePillarGrey.setPosition(gx, gy);
				spriteCoin.setPosition(Cx, Cy);
				level = -7;
				selectLevel = 0;
			}

			if (level == 8 || level == -8 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				ChangeLevel = true;
				sound.play();
				sprite.setPosition(Px, Py);
				spritek.setPosition(Kx, Ky);
				spritek2.setPosition(kx, ky);
				spritep3.setPosition(Gx, Gy);
				spritep2.setPosition(Rx, Ry);
				spritep1.setPosition(Sx, Sy);
				spritePillarBlue.setPosition(sx, sy);
				spritePillarRed.setPosition(rx, ry);
				spritePillarGrey.setPosition(gx, gy);
				spriteCoin.setPosition(Cx, Cy);
				level = -8;
				selectLevel = 0;
			}

			if (level == 9 || level == -9 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				ChangeLevel = true;
				sound.play();
				sprite.setPosition(Px, Py);
				spritek.setPosition(Kx, Ky);
				spritek2.setPosition(kx, ky);
				spritep3.setPosition(Gx, Gy);
				spritep2.setPosition(Rx, Ry);
				spritep1.setPosition(Sx, Sy);
				spritePillarBlue.setPosition(sx, sy);
				spritePillarRed.setPosition(rx, ry);
				spritePillarGrey.setPosition(gx, gy);
				spriteCoin.setPosition(Cx, Cy);
				level = -9;
				selectLevel = 0;
			}

			if (level == 10 || level == -10 && Keyboard::isKeyPressed(Keyboard::Tab)) {
				ChangeLevel = true;
				sound.play();
				sprite.setPosition(Px, Py);
				spritek.setPosition(Kx, Ky);
				spritek2.setPosition(kx, ky);
				spritep3.setPosition(Gx, Gy);
				spritep2.setPosition(Rx, Ry);
				spritep1.setPosition(Sx, Sy);
				spritePillarBlue.setPosition(sx, sy);
				spritePillarRed.setPosition(rx, ry);
				spritePillarGrey.setPosition(gx, gy);
				spriteCoin.setPosition(Cx, Cy);
				level = -10;
				selectLevel = 0;
			}



			if (spritexposition + 25 > spritep1xposition && spritexposition + 6 < spritep1xposition + 31
				&& spriteyposition + 25 > spritep1yposition && spriteyposition + 6 < spritep1yposition + 31
				|| spritexkposition + 17 > spritep1xposition && spritexkposition + 13 < spritep1xposition + 31
				&& spriteykposition + 17 > spritep1yposition && spriteykposition + 13 < spritep1yposition + 31
				|| spritexk2position + 17 > spritep1xposition && spritexk2position + 13 < spritep1xposition + 31
				&& spriteyk2position + 17 > spritep1yposition && spriteyk2position + 13 < spritep1yposition + 31 ||
				spritexposition + 17 > spritePillarBluexposition && spritexposition + 13 < spritePillarBluexposition + 31
				&& spriteyposition + 17 > spritePillarBlueyposition && spriteyposition + 13 < spritePillarBlueyposition + 31
				|| spritexkposition + 17 > spritePillarBluexposition && spritexkposition + 13 < spritePillarBluexposition + 31
				&& spriteykposition + 17 > spritePillarBlueyposition && spriteykposition + 13 < spritePillarBlueyposition + 31
				|| spritexk2position + 17 > spritePillarBluexposition && spritexk2position + 13 < spritePillarBluexposition + 31
				&& spriteyk2position + 17 > spritePillarBlueyposition && spriteyk2position + 13 < spritePillarBlueyposition + 31
				) {
				spritePillarBlue.setTextureRect(IntRect(32, 0, 32, 32));
				PillarBlueUp = false;
			}
			else { spritePillarBlue.setTextureRect(IntRect(0, 0, 32, 32)); PillarBlueUp = true; }

			if (spritexposition + 17 > spritep2xposition && spritexposition + 13 < spritep2xposition + 31
				&& spriteyposition + 17 > spritep2yposition && spriteyposition + 13 < spritep2yposition + 31
				|| spritexkposition + 17 > spritep2xposition && spritexkposition + 13 < spritep2xposition + 31
				&& spriteykposition + 17 > spritep2yposition&& spriteykposition + 13 < spritep2yposition + 31
				|| spritexk2position + 17 > spritep2xposition && spritexk2position + 13 < spritep2xposition + 31
				&& spriteyk2position + 17 > spritep2yposition && spriteyk2position + 13 < spritep2yposition + 31 ||
				spritexposition + 17 > spritePillarRedxposition && spritexposition + 13 < spritePillarRedxposition + 31
				&& spriteyposition + 17 > spritePillarRedyposition && spriteyposition + 13 < spritePillarRedyposition + 31
				|| spritexkposition + 17 > spritePillarRedxposition && spritexkposition + 13 < spritePillarRedxposition + 31
				&& spriteykposition + 17 > spritePillarRedyposition && spriteykposition + 13 < spritePillarRedyposition + 31
				|| spritexk2position + 17 > spritePillarRedxposition && spritexk2position + 13 < spritePillarRedxposition + 31
				&& spriteyk2position + 17 > spritePillarRedyposition && spriteyk2position + 13 < spritePillarRedyposition + 31
				) {
				spritePillarRed.setTextureRect(IntRect(32, 32, 32, 32));
				PillarRedUp = false;
			}
			else { spritePillarRed.setTextureRect(IntRect(0, 32, 32, 32)); PillarRedUp = true; }

			if (spritexposition + 17 > spritep3xposition && spritexposition + 13 < spritep3xposition + 31
				&& spriteyposition + 17 > spritep3yposition && spriteyposition + 13 < spritep3yposition + 31
				|| spritexkposition + 17 > spritep3xposition && spritexkposition + 13 < spritep3xposition + 31
				&& spriteykposition + 17 > spritep3yposition && spriteykposition + 13 < spritep3yposition + 31
				|| spritexk2position + 17 > spritep3xposition && spritexk2position + 13 < spritep3xposition + 31
				&& spriteyk2position + 17 > spritep3yposition && spriteyk2position + 13 < spritep3yposition + 31 ||
				spritexposition + 17 > spritePillarGreyxposition && spritexposition + 13 < spritePillarGreyxposition + 31
				&& spriteyposition + 17 > spritePillarGreyyposition && spriteyposition + 13 < spritePillarGreyyposition + 31
				|| spritexkposition + 17 > spritePillarGreyxposition && spritexkposition + 13 < spritePillarGreyxposition + 31
				&& spriteykposition + 17 > spritePillarGreyyposition && spriteykposition + 13 < spritePillarGreyyposition + 31
				|| spritexk2position + 17 > spritePillarGreyxposition && spritexk2position + 13 < spritePillarGreyxposition + 31
				&& spriteyk2position + 17 > spritePillarGreyyposition && spriteyk2position + 13 < spritePillarGreyyposition + 31
				) {
				spritePillarGrey.setTextureRect(IntRect(32, 64, 32, 32));
				PillarGreyUp = false;
			}
			else { spritePillarGrey.setTextureRect(IntRect(0, 64, 32, 32)); 	PillarGreyUp = true; }


			if (allowCollision == true) {
				if (sprite2Bounds.intersects(spritekBounds)) {

					if (HeroDirection == 0) spritek.move(-1, 0);
					if (HeroDirection == 1) spritek.move(1, 0);
					if (HeroDirection == 2) spritek.move(0, -1);
					if (HeroDirection == 3) spritek.move(0, 1);
				}

				if (sprite2Bounds.intersects(spritek2Bounds)) {

					if (HeroDirection == 0) spritek2.move(-1, 0);
					if (HeroDirection == 1) spritek2.move(1, 0);
					if (HeroDirection == 2) spritek2.move(0, -1);
					if (HeroDirection == 3) spritek2.move(0, 1);
				}


				if (spritekBounds.intersects(spritek2Bounds))
				{
					if (HeroDirection == 0 && spritexkposition>spritexk2position) spritek.move(1, 0);
					if (HeroDirection == 1 && spritexkposition<spritexk2position) spritek.move(-1, 0);
					if (HeroDirection == 2 && spriteykposition>spriteyk2position) spritek.move(0, 1);
					if (HeroDirection == 3 && spriteykposition<spriteyk2position) spritek.move(0, -1);
					if (HeroDirection == 0 && spritexkposition>spritexk2position) sprite.move(1, 0);
					if (HeroDirection == 1 && spritexkposition<spritexk2position) sprite.move(-1, 0);
					if (HeroDirection == 2 && spritexkposition>spritexk2position) sprite.move(0, 1);
					if (HeroDirection == 3 && spritexkposition<spritexk2position) sprite.move(0, -1);
				}

				if (spritekBounds.intersects(spritek2Bounds))
				{
					if (HeroDirection == 0 && spritexkposition<spritexk2position) spritek2.move(1, 0);
					if (HeroDirection == 1 && spritexkposition>spritexk2position) spritek2.move(-1, 0);
					if (HeroDirection == 2 && spriteykposition<spriteyk2position) spritek2.move(0, 1);
					if (HeroDirection == 3 && spriteykposition>spriteyk2position) spritek2.move(0, -1);
					if (HeroDirection == 0 && spritexkposition<spritexk2position) sprite.move(1, 0);
					if (HeroDirection == 1 && spritexkposition>spritexk2position) sprite.move(-1, 0);
					if (HeroDirection == 2 && spritexkposition<spritexk2position) sprite.move(0, 1);
					if (HeroDirection == 3 && spritexkposition>spritexk2position) sprite.move(0, -1);
				}



				if (HeroDirection == 3 && spriteyposition + 16 >  spritePillarBlueyposition) { allowMoveBlueDown = true; }
				else { allowMoveBlueDown = false; }
				if (HeroDirection == 2 && spriteyposition + 16 <  spritePillarBlueyposition) { allowMoveBlueUp = true; }
				else { allowMoveBlueUp = false; }
				if (HeroDirection == 1 && spritexposition + 16 >  spritePillarBluexposition) { allowMoveBlueRight = true; }
				else { allowMoveBlueRight = false; }
				if (HeroDirection == 0 && spritexposition + 16 <  spritePillarBluexposition) { allowMoveBlueLeft = true; }
				else { allowMoveBlueLeft = false; }

				if (HeroDirection == 3 && spriteyposition + 16 >  spritePillarRedyposition) { allowMoveRedDown = true; }
				else { allowMoveRedDown = false; }
				if (HeroDirection == 2 && spriteyposition + 16 <  spritePillarRedyposition) { allowMoveRedUp = true; }
				else { allowMoveRedUp = false; }
				if (HeroDirection == 1 && spritexposition + 16 >  spritePillarRedxposition) { allowMoveRedRight = true; }
				else { allowMoveRedRight = false; }
				if (HeroDirection == 0 && spritexposition + 16 <  spritePillarRedxposition) { allowMoveRedLeft = true; }
				else { allowMoveRedLeft = false; }

				if (HeroDirection == 3 && spriteyposition + 16 >  spritePillarGreyyposition) { allowMoveGreyDown = true; }
				else { allowMoveGreyDown = false; }
				if (HeroDirection == 2 && spriteyposition + 16 <  spritePillarGreyyposition) { allowMoveGreyUp = true; }
				else { allowMoveGreyUp = false; }
				if (HeroDirection == 1 && spritexposition + 16 >  spritePillarGreyxposition) { allowMoveGreyRight = true; }
				else { allowMoveGreyRight = false; }
				if (HeroDirection == 0 && spritexposition + 16 <  spritePillarGreyxposition) { allowMoveGreyLeft = true; }
				else { allowMoveGreyLeft = false; }




				if (sprite2Bounds.intersects(spritePillarBlueBounds) && PillarBlueUp == true)
				{
					if (HeroDirection == 0 && allowMoveBlueLeft == false) sprite.move(1, 0);
					if (HeroDirection == 1 && allowMoveBlueRight == false) sprite.move(-1, 0);
					if (HeroDirection == 2 && allowMoveBlueUp == false) sprite.move(0, 1);
					if (HeroDirection == 3 && allowMoveBlueDown == false) sprite.move(0, -1);
				}

				if (sprite2Bounds.intersects(spritePillarRedBounds) && PillarRedUp == true)
				{
					if (HeroDirection == 0 && allowMoveRedLeft == false) sprite.move(1, 0);
					if (HeroDirection == 1 && allowMoveRedRight == false) sprite.move(-1, 0);
					if (HeroDirection == 2 && allowMoveRedUp == false) sprite.move(0, 1);
					if (HeroDirection == 3 && allowMoveRedDown == false) sprite.move(0, -1);
				}

				if (sprite2Bounds.intersects(spritePillarGreyBounds) && PillarGreyUp == true)
				{
					if (HeroDirection == 0 && allowMoveGreyLeft == false) sprite.move(1, 0);
					if (HeroDirection == 1 && allowMoveGreyRight == false) sprite.move(-1, 0);
					if (HeroDirection == 2 && allowMoveGreyUp == false) sprite.move(0, 1);
					if (HeroDirection == 3 && allowMoveGreyDown == false) sprite.move(0, -1);
				}





				//collision
				for (int j = 0; j<W; j++)
					for (int i = 0; i < H; i++)

					{

						if (TileMap[i][j] == 'B') {
							tt.setTextureRect(IntRect(0, 0, 32, 32));
							FloatRect spriteBounds = sprite.getGlobalBounds();
							FloatRect tBounds = tt.getGlobalBounds();
							FloatRect spritekBounds = spritek.getGlobalBounds();
							FloatRect spritek2Bounds = spritek2.getGlobalBounds();
							FloatRect spritePillarBlueBounds = spritePillarBlue.getGlobalBounds();
							FloatRect spritePillarRedBounds = spritePillarRed.getGlobalBounds();
							FloatRect spritePillarGreyBounds = spritePillarGrey.getGlobalBounds();



							if (spritekBounds.intersects(tBounds) || spritekBounds.intersects(spritePillarBlueBounds) && PillarBlueUp == true
								|| spritekBounds.intersects(spritePillarRedBounds) && PillarRedUp == true
								|| spritekBounds.intersects(spritePillarGreyBounds) && PillarGreyUp == true)
							{
								if (HeroDirection == 0) spritek.move(1, 0);
								if (HeroDirection == 1) spritek.move(-1, 0);
								if (HeroDirection == 2) spritek.move(0, 1);
								if (HeroDirection == 3) spritek.move(0, -1);
								if (HeroDirection == 0) sprite.move(1, 0);
								if (HeroDirection == 1) sprite.move(-1, 0);
								if (HeroDirection == 2) sprite.move(0, 1);
								if (HeroDirection == 3) sprite.move(0, -1);
							}

							if (spritek2Bounds.intersects(tBounds) || spritek2Bounds.intersects(spritePillarBlueBounds) && PillarBlueUp == true
								|| spritek2Bounds.intersects(spritePillarRedBounds) && PillarRedUp == true
								|| spritek2Bounds.intersects(spritePillarGreyBounds) && PillarGreyUp == true)
							{
								if (HeroDirection == 0) spritek2.move(1, 0);
								if (HeroDirection == 1) spritek2.move(-1, 0);
								if (HeroDirection == 2) spritek2.move(0, 1);
								if (HeroDirection == 3) spritek2.move(0, -1);
								if (HeroDirection == 0) sprite.move(1, 0);
								if (HeroDirection == 1) sprite.move(-1, 0);
								if (HeroDirection == 2) sprite.move(0, 1);
								if (HeroDirection == 3) sprite.move(0, -1);
							}





							if (spriteBounds.intersects(tBounds))
							{
								if (HeroDirection == 0) sprite.move(1, 0);
								if (HeroDirection == 1) sprite.move(-1, 0);
								if (HeroDirection == 2) sprite.move(0, 1);
								if (HeroDirection == 3) sprite.move(0, -1);
							}

							tt.setPosition(j * 32, i * 32);

						}
					}
			}
			window.clear();

			for (int i = 0; i<H; i++)
				for (int j = 0; j<W; j++)
				{
					if (TileMap[i][j] == 'B') st.setTextureRect(IntRect(0, 0, 32, 32));

					if (TileMap[i][j] == '0') st.setTextureRect(IntRect(30, 0, 62, 32));

					if (TileMap[i][j] == ' ' || TileMap[i][j] == 'P' || TileMap[i][j] == 'K' || TileMap[i][j] == 'k' || TileMap[i][j] == 'S' || TileMap[i][j] == 's' || TileMap[i][j] == 'R'
						|| TileMap[i][j] == 'r' || TileMap[i][j] == 'G' || TileMap[i][j] == 'g' || TileMap[i][j] == 'C') st.setTextureRect(IntRect(30, 0, 62, 32));
					st.setPosition(j * 32, i * 32);
					window.draw(st);
				}

			window.draw(spritep3);
			window.draw(spritep2);
			window.draw(spritep1);
			window.draw(spriteCoin);
			window.draw(spritePillarBlue);
			window.draw(spritePillarRed);
			window.draw(spritePillarGrey);
			window.draw(spritek);
			window.draw(spritek2);
			window.draw(sprite);
			window.display();

		}
	}

	return 0;
}
